.. toctree::
   :maxdepth: 2
   :caption: Contents:



Wprowadzenie
============
Docker
--------
Docker jest to platforma wykorzystująca konteneryzację. Konteneryzacja polega na tym, że umożliwia uruchomienie
wybranych procesów aplikacji w wydzielonych kontenerach. Dzięki temu nie ma problemów przy uruchamianiu aplikacji na 
różnych urządzeniach. Każdy kontener posiada wydzielony obszar pamięci, wydzielony obszar na dysku,
na którym znajdują się wszystkie biblioteki potrzebne do uruchomienia aplikacji. 
Docker Compose
------------------
Docker Compose to narzędzie pozwalające na uruchomienie aplikacji w wielu kontenerach. Kontenery są uruchamiane
w określonej kolejności.  Docker compose jest pomocne, gdy mamy wiecej niż jeden plik Dockerfile.
Definicje tworzymy w pliku docker-compose.yml, gdzie za pomocą yaml możemy opisać naszą infrastrukturę.
