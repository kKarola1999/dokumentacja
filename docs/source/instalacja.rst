.. toctree::
   :maxdepth: 2
   :caption: Contents:



Instalacja
===========
Uruchomienie aplikacji
-----------------------
1. Najpierw należy sprawdzić, czy prawidłowo zainstalowaliśmy docker i docker-compose.
2. W folderze z aplikacją tworzymy plik docker-compose.yaml.
3. Budujemy obraz aplikacji za pomocą polecenia.::

	docker-compose build

4. Następnie uruchamiamy kontener.::

	docker-compose up

